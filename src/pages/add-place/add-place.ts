import { Component } from '@angular/core';
import { ModalController, LoadingController, ToastController } from 'ionic-angular';
import { NgForm } from "@angular/forms";
import { SetLocationPage } from "../set-location/set-location";
import { Location } from "../../models/location";
import { Geolocation } from '@ionic-native/geolocation';
import { Camera } from '@ionic-native/camera';
import { PlacesService } from "../../services/places";

@Component({
  selector: 'page-add-place',
  templateUrl: 'add-place.html',
})
export class AddPlacePage {
  location: Location = {
    lat: 40.7624324,
    lng: -73.9759827
  };

  locationIsSet = false;
  imageUrl = '';

  constructor(private modelCtrl: ModalController, 
              private geolocation: Geolocation,
              private loadingCtrl: LoadingController,
              private toastCtrl: ToastController,
              private camera: Camera,
              private placesServices: PlacesService) {}
 onSubmit(form: NgForm) {
   this.placesServices.addPlace(form.value.title, form.value.description, this.location, this.imageUrl);
   form.reset();
   this.location = {
    lat: 40.7624324,
    lng: -73.9759827
   }
   this.imageUrl ='';
   this.locationIsSet = false;
 }

 onOpenMap() {
  const model = this.modelCtrl.create(SetLocationPage, {location: this.location, isSet: this.locationIsSet});
  model.present();
  model.onDidDismiss(
    data => {
      if (data) {
        this.location = data.location;
        this.locationIsSet = true;
      }
    }
  );
 }

 onLocate() {
   const loader = this.loadingCtrl.create({
     content: 'Getting your Location..'
   });
  this.geolocation.getCurrentPosition()
  .then(
    location => {
      this.location.lat = location.coords.latitude;
      this.location.lng = location.coords.longitude;
      this.locationIsSet = true;
    }
  )
  .catch(
    error => {
      loader.dismiss();
        const toast = this.toastCtrl.create({
        message: 'Cloud get location, please pick it manually!',
        duration: 2500
      })
      console.log(error)
      toast.present();
    }
  );
 }
  

 onTakePhoto() {
   this.camera.getPicture({
    encodingType: this.camera.EncodingType.JPEG,
    correctOrientation: true
   }).then(
     imageData => {
       this.imageUrl = imageData;
     }
   )
   .catch(
     error => {
       console.log(error);
     }
   );
 }
}
